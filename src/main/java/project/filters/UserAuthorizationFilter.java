package project.filters;

import javax.servlet.annotation.WebFilter;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebFilter(urlPatterns = "/withAuthorization")
public class UserAuthorizationFilter extends HttpFilter {

    @Override
    public void doFilter(HttpServletRequest request,
                         HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (AuthenticationUtil.isAuthenticate(request)){
            chain.doFilter(request, response);
        }
        else {
            response.sendRedirect("/login");
        }

    }
}
