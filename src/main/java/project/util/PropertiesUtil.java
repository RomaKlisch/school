package project.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class PropertiesUtil {

    private static Properties properties = new Properties();

    static {
        ClassLoader classLoader = PropertiesUtil.class.getClassLoader();
        String fileName = "application.properties";
        try (InputStream propertiesStream = classLoader.getResourceAsStream(fileName)) {
            properties.load(propertiesStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public static String getProperty(String propertyName) {
        return properties.getProperty(propertyName);
    }

}