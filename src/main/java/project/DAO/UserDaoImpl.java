package project.DAO;

import project.mappers.UserMapper;
import project.model.User;
import project.util.JdbcWrapper;

import java.util.List;


public class UserDaoImpl implements UserDao {
    private final static UserDaoImpl USERDAO=new UserDaoImpl();

    private UserDaoImpl(){}

    public static UserDaoImpl getUSERDAO() {
        return USERDAO;
    }
    @Override
    public void saveUser(User user) {
        JdbcWrapper.update("insert into users (firstname,secondname,login,password) values(?,?,?,?)",
                user.getFirstName(),
                user.getSecondName(),
                user.getLogin(),
                user.getPassword());
    }

    @Override
    public User getUser(String login) {
        List<User> user=JdbcWrapper.find("select * from users where login=? ",
                new UserMapper(),
                login);
        return user.isEmpty() ? null : user.get(0);
    }

    @Override
    public User getUser(int id) {
        List<User> user=JdbcWrapper.find("select * from users where login=? ",
                new UserMapper(),
                id);
        return user.isEmpty() ? null : user.get(0);
    }

    @Override
    public boolean isUserExist(String login, String password) {
        return !JdbcWrapper.find("select * from users where login=? and password=?",
                new UserMapper(),
                login,
                password).isEmpty();
    }

    @Override
    public void deleteUser(String login) {
        JdbcWrapper.update("delete * from users where login=? ",
                login);

    }
}
