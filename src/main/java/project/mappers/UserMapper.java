package project.mappers;

import project.model.User;

import java.sql.ResultSet;
import java.sql.SQLException;


public class UserMapper implements Mapper {
    @Override
    public User map(ResultSet resultSet) throws SQLException {
        return new User(resultSet.getInt("id"),
                resultSet.getString("firstname"),
                resultSet.getString("secondname"),
                resultSet.getString("login"),
                resultSet.getString("password"));
    }
}
