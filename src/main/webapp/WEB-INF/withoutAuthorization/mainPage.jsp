<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="title" value="Main Page" />
<jsp:include page="/WEB-INF/shared/_header.jsp" />

<h1>Main Page</h1>

    <a href="/login">Log in</a>
    <a href="/registration">Registration</a>
<div>
    <c:choose>
        <c:when test="${isAuthorized}">
            <form action="group"method="post">
                <div>
                    Name <br><input type="text"name="name" required placeholder="Name group" >
                </div>
                <div>
                    <button type="submit">Create group</button>
                </div>
            </form>
        </c:when>
        <c:otherwise>
            <a href="/login">Authorized for Create group</a>
        </c:otherwise>
    </c:choose>
</div>

<jsp:include page="/WEB-INF/shared/_footer.jsp"/>
